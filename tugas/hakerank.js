function hakerank(kalimat) {
    var result = "";
    var scn = 0;
    var code = ["h", "a", "c", "k", "e", "r", "r", "a", "n", "k"];
    for (let i = 0; i < code.length; i++) {
      for (let j = 0; j < kalimat.length; j++) {
        if (kalimat.charAt(j) == code[i]) {
          scn++;
          break;
        }
      }
    }
    if (scn == code.length) {
      result += "YES";
    } else {
      result += "NO";
    }
    var arr = Array2D(3, 2);
    arr = [
      ["Input:", kalimat],
      ["Contains Hackerrank:", code.join("")],
      ["Result:", result]
    ];
    printHTML(arr);
  }